# **WP PLUGIN ANDROID APP** #

## **Firma de la aplicación** ##

keytool -genkey -v -keystore wp-plugin.keystore -alias wp_plugin -keyalg RSA -keysize 2048 -keypass devwpplugininfuy123 -validity 10000 -storepass infuymobile321

**PASS KEYSTORE**: infuymobile321 **PASS ALIAS**: devwpplugininfuy123

**ARCHIVO**: wp-plugin.keystore

## **Branching model** ##

* **origin/master**: production ready code state
* **origin/dev**: latest delivered development changes for next release (integration branch)
* **dev** should be merged to master when code is ready to be released
* **master** should be tagged with release version to be able to reference it later on if necessary
* Devs should work in a **local feature branch**, branched off **from dev** and then merged back in with **git merge --no-ff feature-1** once it finished

See http://nvie.com/posts/a-successful-git-branching-model/ for more details